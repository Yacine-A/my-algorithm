def bfs(graph, root):
    visited, queue = [], [root]
    while queue:
        vertex = queue.pop(0)
        for w in graph[vertex]:
            if w not in visited:
                visited.append(w)
                queue.append(w)

"""
fonction bfs(liste adjacente , noeud)

visite = []
queue = [noeud]
tant que queue n'est pas vide:
   v = queue.pop(0) #On prend  et supprime le dernier entier de la queu
   pour i allant a liste adjacente[v]:
        si liste adjacente[v][i] n'est pas dans visité:
             visite.append(v)
             queue.append(v)             
"""                